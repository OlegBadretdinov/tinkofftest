//
//  TinkoffTestTaskUITests.swift
//  TinkoffTestTaskUITests
//
//  Created by Oleg Badretdinov on 09.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import XCTest

class TinkoffTestTaskUITests: XCTestCase {
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testNewsSelectingAndBack() {
        let app = XCUIApplication()
        let firstCell = app.cells.firstMatch
        firstCell.tap()
        if app.webViews.count == 0 {
            XCTFail()
        }
        app.navigationBars.buttons.element(boundBy: 0).tap()
        if app.cells.count == 0 {
            XCTFail()
        }
    }
}

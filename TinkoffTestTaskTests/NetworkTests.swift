//
//  NetworkTests.swift
//  TinkoffTestTaskTests
//
//  Created by Oleg Badretdinov on 07.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

class NetworkTests: QuickSpec {
    var networkManager: TTTNetworkManager!
    
    override func spec() {
        self.networkManager = TTTNetworkManager(baseUrl: URL(string: "http://localhost/v1/")!)
        
        describe("Internet connection errors") {
            it("404", closure: {
                waitUntil(timeout: 5, action: { (done) in
                    stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: Data(), statusCode: 404, headers: nil)
                    })
                    
                    self.networkManager.getNewsList(completion: { (news, error) in
                        expect(news).to(beNil())
                        expect(error as? TTTErrors).to(equal(TTTErrors.network))
                        done()
                    })
                    
                })
            })
            
            it("500", closure: {
                waitUntil(timeout: 5, action: { (done) in
                    stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: Data(), statusCode: 500, headers: nil)
                    })
                    
                    self.networkManager.getNewsList(completion: { (news, error) in
                        expect(news).to(beNil())
                        expect(error as? TTTErrors).to(equal(TTTErrors.network))
                        done()
                    })
                })
            })
        }
        
        it("Unknown errors") {
            waitUntil(timeout: 5, action: { (done) in
                stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                    return OHHTTPStubsResponse(data: Data(), statusCode: 200, headers: nil)
                })
                
                self.networkManager.getNewsList(completion: { (news, error) in
                    expect(news).to(beNil())
                    expect(error as? TTTErrors).to(equal(TTTErrors.unknown))
                    done()
                })
            })
        }
        
        describe("Server errors") {
            waitUntil(timeout: 5, action: { (done) in
                let url = Bundle(for: type(of: self)).url(forResource: "ServerError", withExtension: "json")!
                let data = try! Data(contentsOf: url)
                
                stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                    return OHHTTPStubsResponse(data: data, statusCode: 200, headers: nil)
                })
                
                self.networkManager.getNewsList(completion: { (news, error) in
                    expect(news).to(beNil())
                    expect(error as? TTTErrors).to(equal(TTTErrors.invalidRequest(message: "Неизвестный тип запроса news_scontentt")))
                    done()
                })
            })
        }
        
        it("News list") {
            waitUntil(timeout: 5, action: { (done) in
                let url = Bundle(for: type(of: self)).url(forResource: "NewsResponse", withExtension: "json")!
                let data = try! Data(contentsOf: url)
                
                stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                    return OHHTTPStubsResponse(data: data, statusCode: 200, headers: nil)
                })

                self.networkManager.getNewsList(completion: { (news, error) in
                    expect(news).notTo(beNil())
                    if let news = news {
                        expect(news.count).to(equal(805))
                        
                        let first = news.first!
                        expect(first.id).to(equal("9644"))
                        expect(first.name).to(equal("02112017-tinkoff-bank-x-markswebb-rank-report"))
                        expect(first.text).to(equal("Мобильное приложение Тинькофф Банка — лучшее в СНГ по версии Markswebb"))
                        expect(first.bankInfoTypeId).to(equal(2))
                        expect(first.publicationDate.timeIntervalSince1970).to(equal(1509620499))
                    }
                    expect(error).to(beNil())
                    done()
                })
            })
        }
        
        it("News content") {
            waitUntil(timeout: 5, action: { (done) in
                let url = Bundle(for: type(of: self)).url(forResource: "NewsContentResponse", withExtension: "json")!
                let data = try! Data(contentsOf: url)
                
                stub(condition: isHost("localhost"), response: { (_) -> OHHTTPStubsResponse in
                    return OHHTTPStubsResponse(data: data, statusCode: 200, headers: nil)
                })
                
                self.networkManager.getInfo(forNewsId: "9584", completion: { (content, error) in
                    expect(content).notTo(beNil())
                    if let content = content {
                        expect(content.title.bankInfoTypeId).to(equal(2))
                        expect(content.title.text).to(equal("Тинькофф Банк улучшил позиции в рейтинге крупнейших компаний \"Эксперт 400\""))
                        expect(content.title.name).to(equal("27102017-tinkoff-x-expert-400"))
                        expect(content.title.id).to(equal("9584"))
                        expect(content.title.publicationDate.timeIntervalSince1970).to(equal(1509102062))
                        
                        expect(content.bankInfoTypeId).to(equal(2))
                        expect(content.typeId).to(equal(TTTNewsContentTypeId.usual))
                        expect(content.lastModificationDate.timeIntervalSince1970).to(equal(1509091576))
                        expect(content.creationDate.timeIntervalSince1970).to(equal(1509091576))
                        expect(content.content).to(equal("<p>Москва, Россия&nbsp;&mdash; 27 октября 2017&nbsp;г. <br>\n\tТинькофф Банк занял 208&nbsp;место в&nbsp;ежегодном рейтинге крупнейших российских компаний &laquo;Эксперт 400&raquo;, поднявшись на&nbsp;2 позиций по&nbsp;сравнению с&nbsp;предыдущим годом. Повышение позиций банка в&nbsp;одном из&nbsp;наиболее авторитетных российских рейтингов обусловлено ростом выручки более чем на&nbsp;40%, а&nbsp;также троекратным ростом чистой прибыли.</p>\n\n<p>По&nbsp;итогам рейтинга банки удержали второе место по&nbsp;объему выручки. В&nbsp;рейтинге &laquo;Эксперт 400&raquo; представлены 36 кредитных организаций, которые равномерно распределены по&nbsp;размерному ряду. &laquo;Если смотреть на&nbsp;агрегированные индексы, то&nbsp;сектор выглядит солидно и&nbsp;устойчиво: сбалансированное соотношение темпа прироста (4,4%&nbsp;&mdash; это немного, но&nbsp;не&nbsp;спад, как у&nbsp;нефтяников) и&nbsp;хорошей рентабельности (почти 10%&nbsp;&mdash; <nobr>точь-в-точь</nobr> как у&nbsp;нефтяников)&raquo;,&nbsp;&mdash; отмечают представители аналитического центра &laquo;Эксперт&raquo;. Также, по&nbsp;их&nbsp;мнению, результаты рейтинга подтверждают, что среди трудоемких отраслей банки&nbsp;&mdash; бесспорный лидер по&nbsp;производительности труда и&nbsp;удельной прибыли на&nbsp;занятого.</p>\n\n<p>Рейтинг &laquo;Эксперт 400&raquo; крупнейших компаний России по&nbsp;объему реализации продукции подготовлен Аналитическим агентством &laquo;Эксперт&raquo;. Агентство входит в&nbsp;состав группы компаний &laquo;Эксперт&raquo;, являясь одним из&nbsp;его <nobr>информационно-исследовательских</nobr> проектов. Каждый год проект &laquo;Эксперт 400&raquo; выявляет наиболее крупные компании российской экономики и&nbsp;на&nbsp;основе результатов их&nbsp;деятельности анализирует роль крупного бизнеса в&nbsp;экономическом развитии России, его структуру и&nbsp;основные тенденции развития.</p>"))
                    }
                    expect(error).to(beNil())
                    done()
                })
            })
        }
    }
}

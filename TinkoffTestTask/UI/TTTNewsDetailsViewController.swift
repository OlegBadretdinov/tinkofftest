//
//  TTTNewsDetailsViewController.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 06.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit
import WebKit

class TTTNewsDetailsViewController: UIViewController {
    var news: CDNews?
    var webView: WKWebView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupWebView()
    }
    
    fileprivate func setupWebView() {
        self.view.addSubview(self.webView)
        self.webView.frame = self.view.bounds
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if let content = self.news?.content?.content {
            self.webView.loadHTMLString(content, baseURL: nil)
        }
    }
}

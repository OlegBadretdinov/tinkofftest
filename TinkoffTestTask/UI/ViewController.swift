//
//  ViewController.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 31.10.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit
import CoreData

fileprivate enum ViewControllerSegue: String {
    case details = "NewsDetailsSegueIdentifier"
}

class ViewController: UIViewController {
    fileprivate static let cellIdentifier = "NewsCell"
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handlePullToRefresh), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    var manager: TTTNewsManager {
        get {
            return TTTNewsManager.shared
        }
    }
    fileprivate var frc: NSFetchedResultsController<CDNews>!
    
    fileprivate var isLoading: Bool = false {
        didSet {
            self.updateSubviews()
        }
    }
    fileprivate var isEmpty: Bool {
        get {
            return (self.frc.fetchedObjects?.count ?? 0) == 0
        }
    }
    fileprivate let dateFormatter :DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        return dateFormatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupFetchedResultController()
        if self.isEmpty {
            self.refreshControl.beginRefreshing()
            self.updateNews()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    fileprivate func updateSubviews() {
        if self.isLoading {
            self.activityIndicator.startAnimating()
            self.tableView.isUserInteractionEnabled = false
        } else {
            self.activityIndicator.stopAnimating()
            self.tableView.isUserInteractionEnabled = true
        }
    }
    
    fileprivate func setupTableView() {
        self.tableView.addSubview(self.refreshControl)
    }
    
    @objc fileprivate func handlePullToRefresh() {
        self.updateNews()
    }
    
    fileprivate func setupFetchedResultController() {
        self.frc = self.manager.newsListFetchedResultController()
        self.frc.delegate = self
        try? self.frc.performFetch()
    }
    
    fileprivate func updateNews() {
        self.manager.updateNews { [weak self] (success, error) in
            guard let `self` = self else { return }
            self.refreshControl.endRefreshing()
            if !success {
                let alert = UIAlertController(title: "Ошибка", message: error?.localizedDescription ?? "???", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewControllerSegue.details.rawValue, let news = sender as? CDNews, let destination = segue.destination as? TTTNewsDetailsViewController {
            destination.news = news
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.frc?.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ViewController.cellIdentifier, for: indexPath)
        if let news = self.frc.fetchedObjects?[indexPath.row] {
            cell.textLabel?.text = news.text
            cell.detailTextLabel?.text = self.dateFormatter.string(from: news.publicationDate!)
        }
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let news = self.frc.fetchedObjects?[indexPath.row] {
            let completion = {
                self.performSegue(withIdentifier: ViewControllerSegue.details.rawValue, sender: news)
            }
            if news.content != nil {
                completion()
            } else {
                self.isLoading = true
                self.manager.updateContent(forNewsId: news.id!, completion: { [weak self] (success, error) in
                    guard let `self` = self else { return }
                    self.isLoading = false
                    if success {
                        completion()
                    } else {
                        let alert = UIAlertController(title: "Ошибка", message: error?.localizedDescription ?? "???", preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alert.addAction(cancelAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                })
            }
        }
    }
}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.reloadData()
    }
}

//
//  TTTCoreDataManager.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 31.10.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit
import CoreData

class TTTCoreDataManager: NSObject {
//    lazy var persistentContainer: NSPersistentContainer = {
//        let container = NSPersistentContainer(name: "TinkoffTestTask")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }
    static let shared = TTTCoreDataManager()
    
    lazy var docDirectory: URL! = {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    }()

    lazy var model: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "TinkoffTestTask", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.model)
        let url = self.docDirectory.appendingPathComponent("TestTask.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            abort()
        }
        
        return coordinator
    }()
    
    private lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    public fileprivate(set) lazy var mainManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.privateManagedObjectContext
        return managedObjectContext
    }()
    
    func save() {
        self.mainManagedObjectContext.performAndWait {
            if self.mainManagedObjectContext.hasChanges {
                try? self.mainManagedObjectContext.save()
            }
        }
        self.privateManagedObjectContext.performAndWait {
            if self.privateManagedObjectContext.hasChanges {
                try? self.privateManagedObjectContext.save()
            }
        }
    }
    
    func save(withBlock block: (_ context: NSManagedObjectContext)->Void, completion: @escaping ()->Void) {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.mainManagedObjectContext
        
        context.performAndWait {
            block(context)
            
            if context.hasChanges {
                try? context.save()
            }
        }
        
        DispatchQueue.main.async(execute: completion)
    }
}

//
//  CDNewsExtension.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 05.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import Foundation

extension CDNews {
    func importFromTTTNews(_ news: TTTNews) {
        self.id = news.id
        self.name = news.name
        self.text = news.text
        self.bankInfoTypeId = news.bankInfoTypeId
        self.publicationDate = news.publicationDate
    }
}

extension CDNewsContent {
    func importFromTTTNewsContent(_ content: TTTNewsContent) {
        self.lastModificationDate = content.lastModificationDate
        self.creationDate = content.creationDate
        self.content = content.content
        self.bankInfoTypeId = content.bankInfoTypeId
        self.typeId = content.typeId.rawValue
    }
}

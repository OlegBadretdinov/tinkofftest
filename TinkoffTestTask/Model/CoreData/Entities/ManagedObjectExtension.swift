//
//  ManagedObjectExtension.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 05.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    func newObject<T: NSManagedObject>(ofType type: NSManagedObject.Type) -> T {
        return  T(entity: NSEntityDescription.entity(forEntityName: String(describing: T.self), in: self)!, insertInto: self)
    }
    
    func deleteObjects<T: NSManagedObject>(withPredicate predicate: NSPredicate?, ofType type: T.Type) {
        let objects :[T] = self.findObjects(withPredicate: predicate) ?? []
        for object in objects {
            self.delete(object)
        }
    }
    
    func findFirst<T: NSManagedObject>(withPredicate predicate: NSPredicate?, ofType type: T.Type) -> T? {
        let request = NSFetchRequest<T>(entityName: String(describing: T.self))
        request.predicate = predicate
        let result = (try? self.fetch(request)) ?? []
        return result.first
    }
    
    func findObjects<T: NSManagedObject>(withPredicate predicate: NSPredicate?) -> [T]? {
        let request = NSFetchRequest<T>(entityName: String(describing: T.self))
        request.predicate = predicate
        return try? self.fetch(request)
    }
}

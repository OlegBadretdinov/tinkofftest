//
//  TTTRootResponse.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 31.10.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit

enum ResultCode : String, Codable {
    case OK = "OK"
    case invalidRequest = "INVALID_REQUEST_DATA"
    case internalError = "INTERNAL_ERROR"
}

struct TTTRootResponse<Payload: Codable>: Codable {
    let resultCode: ResultCode
    let errorMessage: String!
    let plainMessage: String!
    let payload: Payload!
    let trackingId: String!
}

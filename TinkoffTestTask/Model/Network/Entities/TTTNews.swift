//
//  TTTNews.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 31.10.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit

struct TTTNews: Codable {
    let id: String
    let name: String
    let text: String
    let publicationDate: Date
    let bankInfoTypeId: Int16
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case text
        case bankInfoTypeId
        case publicationDate
    }
    
    enum PublicationDateKeys: String, CodingKey {
        case milliseconds
    }
}

extension TTTNews {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decode(String.self, forKey: .id)
        self.name = try values.decode(String.self, forKey: .name)
        self.text = try values.decode(String.self, forKey: .text)
        self.bankInfoTypeId = try values.decode(Int16.self, forKey: .bankInfoTypeId)
        
        let publicationDateContainer = try values.nestedContainer(keyedBy: PublicationDateKeys.self, forKey: .publicationDate)
        self.publicationDate = try publicationDateContainer.decode(Date.self, forKey: .milliseconds)
    }
}


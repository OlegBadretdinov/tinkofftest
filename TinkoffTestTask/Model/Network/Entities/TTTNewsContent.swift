//
//  TTTNewsContent.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 01.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit

enum TTTNewsContentTypeId : String, Codable {
    case usual = "usual"
    case fin = "fin"
}

struct TTTNewsContent: Codable {
    let title: TTTNews
    let lastModificationDate: Date
    let creationDate: Date
    let content: String
    let bankInfoTypeId: Int16
    let typeId: TTTNewsContentTypeId
    
    enum CodingKeys: String, CodingKey {
        case title
        case lastModificationDate
        case creationDate
        case content
        case bankInfoTypeId
        case typeId
    }
    
    enum DateKeys: String, CodingKey {
        case milliseconds
    }
}

extension TTTNewsContent {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.title = try values.decode(TTTNews.self, forKey: .title)
        self.content = try values.decode(String.self, forKey: .content)
        self.bankInfoTypeId = try values.decode(Int16.self, forKey: .bankInfoTypeId)
        self.typeId = try values.decode(TTTNewsContentTypeId.self, forKey: .typeId)
        
        let lastModificationContainer = try values.nestedContainer(keyedBy: DateKeys.self, forKey: .lastModificationDate)
        self.lastModificationDate = try lastModificationContainer.decode(Date.self, forKey: .milliseconds)
        
        let creationDateContainer = try values.nestedContainer(keyedBy: DateKeys.self, forKey: .creationDate)
        self.creationDate = try creationDateContainer.decode(Date.self, forKey: .milliseconds)
    }
}

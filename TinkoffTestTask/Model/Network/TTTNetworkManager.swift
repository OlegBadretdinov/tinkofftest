//
//  TTTNetworkManager.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 31.10.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import Foundation

enum TTTErrors: Error, Equatable {
    case network
    case unknown
    case internalError(message: String)
    case invalidRequest(message: String)
    
    static func ==(lhs: TTTErrors, rhs: TTTErrors) -> Bool {
        switch (lhs, rhs) {
        case (.network, .network), (.unknown, .unknown):
            return true
        case (.internalError(message: let lMessage), .internalError(message: let rMessage)):
            return lMessage == rMessage
        case (.invalidRequest(message: let lMessage), .invalidRequest(let rMessage)):
            return lMessage == rMessage
        default:
            return false
        }
    }
}

fileprivate enum HttpMethod: String {
    case GET = "GET"
    case POST = "POST"
    
    var isParamsInUrl: Bool {
        return self == .GET
    }
}

fileprivate enum Path: String {
    case news = "news"
    case newsInfo = "news_content"
}

fileprivate enum Parameters: String {
    case id = "id"
}

enum NetworkError: Error {
    case badUrl
    case unknown
    case badResponse
}

class TTTNetworkManager: NSObject {
    fileprivate var baseUrl: URL
    fileprivate var decoder :JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        return decoder
    }()
    
    init(baseUrl :URL) {
        self.baseUrl = baseUrl
    }
    
    fileprivate func performRequest<T: Codable>(httpMethod: HttpMethod, path: Path, parameters :[Parameters : String]!, completion :@escaping (_ responseObject: T?, _ error: Error?) -> Void) {
        let url = self.baseUrl.appendingPathComponent(path.rawValue)
        
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            completion(nil, NetworkError.badUrl)
            return
        }
        
        var queryItems: [URLQueryItem] = []
        var httpBody: Data?
        
        if let parameters = parameters {
            if httpMethod.isParamsInUrl {
                parameters.forEach({ (key, value) in
                    queryItems.append(URLQueryItem(name: key.rawValue, value: value))
                })
            } else {
                let strArray = parameters.map({ (key, value) -> String in
                    return "\(key.rawValue)=\(value)"
                })
                
                httpBody = strArray.joined(separator: "&").data(using: String.Encoding.utf8)
            }
        }
        components.queryItems = queryItems.count > 0 ? queryItems : nil
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = httpMethod.rawValue
        request.httpBody = httpBody
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            var internalError: TTTErrors!
            if let data = data, let root = try? self.decoder.decode(TTTRootResponse<T>.self, from: data) {
                switch root.resultCode {
                case .internalError:
                    internalError = .internalError(message: root.plainMessage)
                case .invalidRequest:
                    internalError = .invalidRequest(message: root.plainMessage)
                default:
                    break
                }
                
                completion(root.payload, internalError)
            } else {
                if let response = response as? HTTPURLResponse, response.statusCode != 200 {
                    internalError = .network
                } else if let _ = error {
                    internalError = .network
                } else {
                    internalError = .unknown
                }
                completion(nil, internalError)
            }
        }.resume()
    }
}

extension TTTNetworkManager {
    func getNewsList(completion :@escaping (_ news :[TTTNews]?, _ error: Error?) -> Void) {
        self.performRequest(httpMethod: HttpMethod.GET, path: Path.news, parameters: nil, completion: completion)
    }
    
    func getInfo(forNewsId newsId :String, completion :@escaping (_ news :TTTNewsContent?, _ error: Error?) -> Void) {
        self.performRequest(httpMethod: HttpMethod.POST, path: Path.newsInfo, parameters: [.id : newsId], completion: completion)
    }
}

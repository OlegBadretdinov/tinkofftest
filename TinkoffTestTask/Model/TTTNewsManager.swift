//
//  TTTNewsManager.swift
//  TinkoffTestTask
//
//  Created by Oleg Badretdinov on 05.11.2017.
//  Copyright © 2017 Oleg Badretdinov. All rights reserved.
//

import UIKit
import CoreData

class TTTNewsManager: NSObject {
    static let shared = TTTNewsManager(networkManager: TTTNetworkManager(baseUrl: URL(string: "https://api.tinkoff.ru/v1")!), coreDataManager: TTTCoreDataManager.shared)
    
    fileprivate let networkManager: TTTNetworkManager
    fileprivate let coreDataManager: TTTCoreDataManager
    
    init(networkManager: TTTNetworkManager, coreDataManager: TTTCoreDataManager) {
        self.networkManager = networkManager
        self.coreDataManager = coreDataManager
    }
    
    func newsListFetchedResultController() -> NSFetchedResultsController<CDNews> {
        let fetchRequest = NSFetchRequest<CDNews>(entityName: String(describing: CDNews.self))
        let sort = NSSortDescriptor(key: #keyPath(CDNews.publicationDate), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.coreDataManager.mainManagedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    
    func updateNews(completion: @escaping (_ success: Bool, _ error: Error?)->Void) {
        self.networkManager.getNewsList { [weak self] (newsList, error) in
            guard let `self` = self else { return }
            if let newsList = newsList {
                self.coreDataManager.save(withBlock: { (context) in
                    var list = [CDNews]()
                    for news in newsList {
                        let predicate = NSPredicate(format: "id == %@", news.id)
                        let cdNews :CDNews = context.findFirst(withPredicate: predicate, ofType: CDNews.self) ?? context.newObject(ofType: CDNews.self)
                        cdNews.importFromTTTNews(news)
                        list.append(cdNews)
                    }
                    context.deleteObjects(withPredicate: NSPredicate(format: "NOT self in %@", list), ofType: CDNews.self)
                }, completion: {
                    completion(true, nil)
                })
            } else {
                completion(false, error)
            }
        }
    }
    
    func updateContent(forNewsId id: String, completion: @escaping (_ success: Bool, _ error: Error?)->Void) {
        self.networkManager.getInfo(forNewsId: id) { (content, error) in
            if let content = content {
                self.coreDataManager.save(withBlock: { (context) in
                    if let cdNews :CDNews = context.findFirst(withPredicate: NSPredicate(format: "id == %@", id), ofType: CDNews.self) {
                        let cdContent :CDNewsContent = cdNews.content ?? context.newObject(ofType: CDNewsContent.self)
                        cdContent.title = cdNews
                        cdContent.importFromTTTNewsContent(content)
                    }
                }, completion: {
                    completion(true, nil)
                })
            } else {
                completion(false, error)
            }
        }
    }
}
